#include <iostream>
#include <string>
#include "professor.hpp"

Professor::Professor(){
	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula_F(0);
	setCarga(0);
	setSalario(0.0);
	setDisciplina("");
	setProjeto(0);
}

Professor::Professor(string nome, string idade, string telefone, int matricula_func, int carga_horaria,float salario, string disciplina, bool projeto){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula_F(matricula_func);
	setCarga(carga_horaria);
	setSalario(salario);
	setDisciplina(disciplina);
	setProjeto(projeto);
}

void Professor::setMatricula_F(int matricula_func){
	this->matricula_func = matricula_func;
}

int Professor::getMatricula_F(){
	return matricula_func;
}

void Professor::setCarga(int carga_horaria){
	this->carga_horaria = carga_horaria;
}

int Professor::getCarga(){
	return carga_horaria;
}

void Professor::setSalario(float salario){
	this->salario = salario;
}

float Professor::getSalario(){
	return salario;
}

void Professor::setDisciplina(string disciplina){
	this->disciplina = disciplina;
}

string Professor::getDisciplina(){
	return disciplina;
}

void Professor::setProjeto(bool projeto){
	this->projeto = projeto;
}

bool Professor::getProjeto(){
	return projeto;
}
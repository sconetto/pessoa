#include <iostream>
#include "aluno.hpp"

Aluno::Aluno(){
	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula(0);
	setQuantidadeCreditos(0);
	setSemestre(0);
	setIra(0.0);
}

Aluno::Aluno(string nome, string idade, string telefone, int matricula, int quantidade_de_creditos, int semestre, float ira){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
	setQuantidadeCreditos(quantidade_de_creditos);
	setSemestre(semestre);
	setIra(ira);
}

void Aluno::setMatricula(int matricula){
	this->matricula = matricula;
}

int Aluno::getMatricula(){
	return matricula;
}

void Aluno::setQuantidadeCreditos(int creditos){
	quantidade_de_creditos = creditos;
}

int Aluno::getQuantidadeCreditos(){
	return quantidade_de_creditos;
}

void Aluno::setSemestre(int semestre){
	this->semestre = semestre;
}

int Aluno::getSemestre(){
	return semestre;
}

void Aluno::setIra(float ira){
	this->ira = ira;
}

float Aluno::getIra(){
	return ira;
}
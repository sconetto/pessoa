#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;

int main(int argc, char ** argv) {
	Pessoa aluno_1;
	Pessoa *aluno_2;
	Pessoa *aluno_3;
	Aluno *aluno_4;
	Professor *prof_1;
	
	aluno_1.setNome("Bruno");
	aluno_1.setTelefone("555-4444");
	aluno_1.setIdade("14");

	aluno_2 = new Pessoa(); 

	aluno_2->setNome("Maria");
	aluno_2->setTelefone("333-5555");
	aluno_2->setIdade("54");

	aluno_3 = new Pessoa("Joao","35","222-5555");
	aluno_4 = new Aluno();
	aluno_4->setNome("Lucas");
	aluno_4->setIdade("22");
	aluno_4->setTelefone("555-1234");
	aluno_4->setMatricula(101032);
	aluno_4->setQuantidadeCreditos(58);
	aluno_4->setSemestre(4);
	aluno_4->setIra(3.5);


	prof_1 = new Professor();
	//"Renato","30","555-4332","101039","44","5900.12","Orientacao a Objetos","1"
	prof_1->setNome("Renato");
	prof_1->setIdade("30");
	prof_1->setTelefone("555-4332");
	prof_1->setMatricula_F(101039);
	prof_1->setCarga(44);
	prof_1->setSalario(5900.12);
	prof_1->setDisciplina("Orientacao a Objetos");
	prof_1->setProjeto(1);

	cout << "Nome\tIdade\tTelefone\tMatricula\tCreditos\tSemestre\tIra" << endl;
	cout << aluno_4->getNome() << "\t" << aluno_4->getIdade() << "\t" << aluno_4->getTelefone() << "\t" << aluno_4->getMatricula() << "\t\t" << aluno_4->getQuantidadeCreditos() << "\t\t" << aluno_4->getSemestre() << "\t\t" << aluno_4->getIra() << endl;
	cout << "Nome\tIdade\tTelefone\tMatricula Funcional\tCarga Horaria\tSalario\t\tDisciplina\t\tProjeto" << endl;
	cout << prof_1->getNome() << "\t" << prof_1->getIdade() << "\t" << prof_1->getTelefone() << "\t" << prof_1->getMatricula_F() << "\t\t\t" << prof_1->getCarga() << "\t\t" << prof_1->getSalario() << "\t\t" << prof_1->getDisciplina() << "\t" << prof_1->getProjeto() << endl;
	delete(aluno_2);
	delete(aluno_3);
	delete(aluno_4);
	delete(prof_1);

}

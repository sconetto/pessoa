#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

class Professor: public Pessoa{
	private:
		int matricula_func;
		int carga_horaria;
		float salario;
		string disciplina;
		bool projeto;
	public:
		Professor();
		Professor(string nome, string idade, string telefone, int matricula_func, int carga_horaria,float salario, string disciplina, bool projeto);
        int getMatricula_F();
        void setMatricula_F(int matricula_func);
        int getCarga();
        void setCarga(int carga_horaria);
        float getSalario();
        void setSalario(float salario);
        string getDisciplina();
        void setDisciplina(string disciplina);
        bool getProjeto();
        void setProjeto(bool projeto);
};


#endif
